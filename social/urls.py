from django.urls import path
from social_django.views import auth as socia_auth_view
from social.views import exchange_token, google_page


urlpatterns = [
	path('google/', google_page, name="google_page"),
	path('<backend>/', exchange_token, name='success_tocken'),
]