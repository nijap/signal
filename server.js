//requires
const express = require('express');
const app = express();
var port = process.env.PORT || 3001;
var http = require('http').Server(app);
var io = require('socket.io')(http);
var users = {};
var userlist = [];
var i;
// express routing
app.use(express.static('public'));

function listuser(temp) {
    var str1 = userlist[temp].userId + " and socketId is " + userlist[temp].socketId;
    // console.log(str1);
};


// signaling
io.on('connection', function(socket) {
    console.log('a user connected');

    socket.on('create or join', function(room, video) {
        // console.log('create or join to room ', room);
        console.log("video : ", video);
        var myRoom = io.sockets.adapter.rooms[room] || { length: 0 };
        var numClients = myRoom.length;
        console.log(room, ' has ', numClients, ' clients');
        if (numClients == 0) {
            console.log(' numClients : ', numClients);
            socket.join(room);
            socket.emit('created', room, video);
        } else if (numClients == 1 || numClients == 2) {
            console.log(' numClients : ', numClients);
            socket.join(room);
            socket.emit('joined', room, video);
            // socket.emit('created', room, video);
        } else {
            socket.emit('full', room);
        }
    });

    socket.on('ready', function(room) {
        console.log("socket.broadcast.to(room) : ", room)
        socket.broadcast.to(room).emit('ready');
    });

    socket.on('candidate', function(event) {
        socket.broadcast.to(event.room).emit('candidate', event);
    });

    socket.on('offer', function(event) {
        socket.broadcast.to(event.room).emit('offer', event.sdp);
    });

    socket.on('answer', function(event) {
        socket.broadcast.to(event.room).emit('answer', event.sdp);
    });

    socket.on('toggleAudio', function(event) {
        socket.broadcast.to(event.room).emit('toggleAudio', event.message);
    });

    socket.on('node_room', function(event) {
        // socket.broadcast.to(event.room).emit('toggleAudio', event.message);
        // console.log("inside node_room");
        for (i = 0; i < userlist.length; i++) {
            if (userlist[i].userId == event.callee) {
                var message = {message:'Call from' + event.caller,room: event.room, video:event.video}
                socket.to(userlist[i].socketId).emit('user_call', message);
            }
        }
    });

    socket.on('login', function(data) {
        //saving userId to array with socket ID
        console.log("user " + data.userId + " connected to room " + data.room)
        userlist.push({
            userId: data.userId,
            socketId: socket.id
        });
        users[socket.id] = data.userId;
        for (i = 0; i < userlist.length; i++) {
            listuser(i);
        }
        io.local.emit('user_joined', { userlist: userlist, message: " connected to this room" });
    });
    socket.on('disconnect', function() {
        console.log('user ' + users[socket.id] + ' disconnected');
        socket.emit('disconnected');
        removeFromList(socket.id);
        for (i = 0; i < userlist.length; i++) {
            listuser(i);
        }
        io.local.emit('user_left', { userlist: userlist, message: " disconnected from this room" });
    });

    socket.on('stopCall', function(event) {
        console.log('stopCall user  ' + users[socket.id] + ' disconnected');
        removeFromList(socket.id);
        for (i = 0; i < userlist.length; i++) {
            listuser(i);
        }
        socket.broadcast.to(event.room).emit('disconnectCall', event.message);
        socket.leave(event.room);
    });
    
    socket.on('disconnectClient', function(event) {
        console.log('disconnectClient ' + users[socket.id] + ' disconnected');
        removeFromList(socket.id);
        for (i = 0; i < userlist.length; i++) {
            listuser(i);
        }
        // socket.leave("3");
    });


});

function removeFromList(sid) {
    console.log("removeFromList : ", sid)
    console.log(userlist);
    for (var i in userlist) {
        if (userlist[i].socketId == sid) {
            userlist.splice(i, 1);
        }
    }
    console.log(userlist);
    return;
}

// listener
http.listen(port, function() {
    console.log('listening on *:',port);
});