// getting dom elements
var divSelectRoom = document.getElementById("selectRoom");
var divConferenceRoom = document.getElementById("conferenceRoom");
// var btnGoBoth = document.getElementById("goBoth");
var btnGoVideoOnly = document.getElementById("goVideoOnly");
var btnGoAudioOnly = document.getElementById("goAudioOnly");
var localVideo = document.getElementById("localVideo");
var remoteVideo = document.getElementById("remoteVideo");
var btnMute = document.getElementById("mute");
var listAudioEvents = document.getElementById("audioEvents");

// variables
var roomNumber
var localStream;
var remoteStream;
var rtcPeerConnection;
var iceServers = {
    'iceServers': [{
            'url': 'stun:stun.services.mozilla.com'
        },
        {
            'url': 'stun:stun.l.google.com:19302'
        }
    ]
}
var streamConstraints;
var isCaller;

// Let's do this
var socket = io('69.164.196.222:3001');

function callbtn() {
    document.getElementById("call-btn").onclick = () => initiateCall(true, true);
}
function answerbtn() {
    document.getElementById("answer-btn").onclick = () => initiateCall(true, true);
}
// btnGoBoth.onclick = () => initiateCall(true, true);
// btnGoVideoOnly.onclick = () => initiateCall(true, false);
// btnGoAudioOnly.onclick = () => initiateCall(false, true);
// btnMute.onclick = toggleAudio;

const log = (message, options) => {
    console.log("message in log is: " + message);
    listtoli(message);
}

function initiateCall(video, audio, roomNumber) {
    console.log("initiate call ")
    window.streamConstraints = {
        video: video,
        audio: {
            echoCancellation: { exact: true },
            mozEchoCancellation: { exact: true },
            mozAutoGainControl: { exact: true },
            mozNoiseSuppression: { exact: true },
        }
    }
    window.roomNumber = roomNumber;
    console.log("ROOOOOOM : ", roomNumber)
    socket.emit('create or join', roomNumber, video);
    // divSelectRoom.style = "display: none;"; //btns not using now
    divConferenceRoom.style = "display: block;";
}

function initiateChat(roomNumber='public_room'){
    window.roomNumber = roomNumber;
    socket.emit('create or join chat', roomNumber);
}

// message handlers
socket.on('created', function(room, video) {
    console.log(" client created video : ", video);
    navigator.mediaDevices.getUserMedia({audio:true, video:video}).then(function(stream) {
        addLocalStream(stream);
        isCaller = true;
    }).catch(function(err) {
        console.log('An error ocurred when accessing media devices');
    });
});

socket.on('joined', function(room, video) {
    console.log(" client joined video : ", video);
    navigator.mediaDevices.getUserMedia({audio:true, video:video}).then(function(stream) {
        addLocalStream(stream);
        // alert("room in joined: "+room);
        socket.emit('ready', room);
    }).catch(function(err) {
        console.log('An error ocurred when accessing media devices');
    });
});

socket.on('disconnected', function(room, video) {
    // alert("call disconnected");
});

socket.on('candidate', function(event) {
    var candidate = new RTCIceCandidate({
        sdpMLineIndex: event.label,
        candidate: event.candidate
    });
    rtcPeerConnection.addIceCandidate(candidate);
});

socket.on('ready', function() {
    console.log("isCaller : ", isCaller);
    if (isCaller) {
        console.log("done");
        createPeerConnection();
        let offerOptions = {
            offerToReceiveAudio: 1
        }
        rtcPeerConnection.createOffer(offerOptions)
            .then(desc => setLocalAndOffer(desc))
            .catch(e => console.log(e));
    }
});

socket.on('offer', function(event) {
    if (!isCaller) {
        createPeerConnection();
        rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
        rtcPeerConnection.createAnswer()
            .then(desc => setLocalAndAnswer(desc))
            .catch(e => console.log(e));
    }
});                            

socket.on('answer', function(event) {
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
})

socket.on('toggleAudio', function(event) {
    notifyEvent(event);
});

socket.on('user_left', (data) => {
    // console.log("testing.......");
    log(data.userlist);
});

socket.on('user_joined', (data) => {
    // console.log("testing......");
    log(data.userlist);
});

socket.on('user_call', (data) => {
    alert_call(data);
});

socket.on('disconnectCall', function(event) {
    isCaller = '';
    divConferenceRoom.style = "display: none;";
    localStream.stop();    
    $("#callAlert").text("call disconected ");
    $("#callAlert").show();
    $("#stopCall").hide();
    socket.emit('disconnectClient', {
        type: 'disconnectClient',
        room: roomNumber,
        message: "Disconnect Client"
    });
    // socket.emit('login', { userId: YourUserID, room: roomNumber });
    // console.log(" Login Again ");
    $('#call-btn-audio').prop("disabled", false);
    $('#call-btn-video').prop("disabled", false);
});

// handler functions
function onIceCandidate(event) {
    if (event.candidate) {
        console.log('sending ice candidate : ', roomNumber);
        socket.emit('candidate', {
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate,
            room: roomNumber
        })
    }
}

function onAddStream(event) {
    console.log(" ---------- onAddStream --------- ");
    remoteVideo.src = URL.createObjectURL(event.stream);
    remoteStream = event.stream;
    if (remoteStream.getAudioTracks().length > 0) {
        notifyEvent('Remote user is sending Audio');
    } else {
        notifyEvent('Remote user is not sending Audio');
    }
}

function setLocalAndOffer(sessionDescription) {
    console.log("offer local offer :", roomNumber);
    rtcPeerConnection.setLocalDescription(sessionDescription);
    socket.emit('offer', {
        type: 'offer',
        sdp: sessionDescription,
        room: roomNumber
    });
}

function setLocalAndAnswer(sessionDescription) {
    console.log("set local answer : ",roomNumber);
    rtcPeerConnection.setLocalDescription(sessionDescription);
    socket.emit('answer', {
        type: 'answer',
        sdp: sessionDescription,
        room: roomNumber
    });
}

// utility functions
function addLocalStream(stream) {
    localStream = stream;
    localVideo.src = URL.createObjectURL(stream);

    if (stream.getAudioTracks().length > 0) {
        btnMute.style = "display: block";
    }
}

function createPeerConnection() {
    rtcPeerConnection = new RTCPeerConnection(iceServers);
    rtcPeerConnection.onicecandidate = onIceCandidate;
    rtcPeerConnection.onaddstream = onAddStream;
    rtcPeerConnection.addStream(localStream);
}

function toggleAudio() {
    localStream.getAudioTracks()[0].enabled = !localStream.getAudioTracks()[0].enabled
    socket.emit('toggleAudio', {
        type: 'toggleAudio',
        room: roomNumber,
        message: localStream.getAudioTracks()[0].enabled ? "Remote user's audio is unmuted" : "Remote user's audio is muted"
    });
}

function toggleVideo() {
    localStream.getVideoTracks()[0].enabled = !localStream.getVideoTracks()[0].enabled
    socket.emit('toggleVideo', {
        type: 'toggleVideo',
        room: roomNumber,
        message: localStream.getVideoTracks()[0].enabled ? "Remote user's video is unmuted" : "Remote user's video is muted"
    });
}

function node_room(room_name, caller_id, callee_id, video) {
    socket.emit('node_room', {
        type: 'node_room',
        room: room_name,
        caller: caller_id,
        callee: callee_id,
        video:video
    });
}

function notifyEvent(event) {
    var p = document.createElement("p");
    p.appendChild(document.createTextNode(event));
    listAudioEvents.appendChild(p);
    console.log(event);
}

function stopCall() {
    isCaller = '';
    console.log("stop call to server ------->",roomNumber);
    $("#callAlert").hide();
    $("#stopCall").hide();    
    socket.emit('stopCall', {
        type: 'stopCall',
        room: roomNumber,
        message: "stop call"
    });
    localStream.stop();
    divConferenceRoom.style = "display: none;";
    // socket.emit('login', { userId: YourUserID, room: roomNumber });
    // console.log(" Login Again ");
    $('#call-btn-audio').prop("disabled", false);
    $('#call-btn-video').prop("disabled", false);
}