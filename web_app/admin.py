from django.contrib import admin
from django.contrib.sessions.models import Session
from web_app.models import *
# Register your models here.

admin.site.register(Member)
admin.site.register(ActivationKey)
admin.site.register(Session)
admin.site.register(InviteMember)
admin.site.register(ChatRoom)
admin.site.register(ChatHistory)