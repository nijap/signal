from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
# from rest_framework.validators import UniqueValidator
# from accounts.models import MyUser
# from django.contrib.auth import authenticate
# from django.utils.translation import ugettext_lazy as _
# from django.shortcuts import get_object_or_404
# from django.contrib.auth.forms import PasswordResetForm
from django.conf import settings
from web_app.models import *
from accounts.models import *


class ChatRoomSerializer(serializers.Serializer):
    chatroom = serializers.CharField(min_length=20)
    remote_user = serializers.CharField(min_length=20)


class ChatHistorySerializer(ModelSerializer):
	"""
	Serializer for Chat Histroy Groups.
	"""
	
	class Meta:
		model = ChatHistory
		fields = ['text','date', 'user']


class UserListSerializer(ModelSerializer):
	"""
	Serializer for Users list.
	"""
	class Meta:
		model = MyUser
		fields = ['id', 'email']


class CreateRoomSerializer(ModelSerializer):
	room_name = serializers.CharField()

	class Meta:
		model = ChatRoom
		fields = ['room_name', 'id']

	def validate_room_name(self, value):

		# Check to see room exist or not.
		try:
			chatroom = ChatRoom.objects.get(room_name=value)
			if chatroom:
				raise serializers.ValidationError( "Room exist already '"+str(value)+"'")
		except ChatRoom.DoesNotExist:			
			return value


class RoomDetailSerializer(ModelSerializer):
	class Meta:
		model = ChatRoom
		fields = ['id','room_name', 'members']
		lookup_field = 'id'


class RoomMessagesSerializer(ModelSerializer):

	user = UserListSerializer()
	
	class Meta:
		model = ChatHistory
		fields = ['text','date', 'user']


class AddToRoomSerializer(serializers.Serializer):
	room_id = serializers.CharField()
	user_id = serializers.CharField()

	def validate_room_id(self, value):

		# Check to see room exist or not.
		try:
			chatroom = ChatRoom.objects.get(id=value)
			if chatroom:
				return value				
		except ChatRoom.DoesNotExist:
			raise serializers.ValidationError( "Room not exist with id '"+str(value)+"'")
			
	def validate_user_id(self, value):

		# Check to see user exist or not.
		try:
			user = MyUser.objects.get(id=value)
			if user:
				return value				
		except MyUser.DoesNotExist:			
			raise serializers.ValidationError( "User not exist with this id '"+str(value)+"'")