from django.conf.urls import url, include
from django.urls import path, re_path
from web_app.views import *
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from rest_framework import routers

router = routers.DefaultRouter()
# router.register('chat-history', ChatHistory, base_name='chat-history')
router.register('users-list', UsersList, base_name='users-list')
router.register('room-create', RoomCreateView, base_name='create-room')
router.register('room-detail', RoomDetailView, base_name='room-detail')
router.register('room-messages', RoomMessagesView, base_name='room-messages')

urlpatterns = [
	re_path(r'', include(router.urls)),
    path('verify-user-key/<slug:user_key>/', VerifyUserKeyView.as_view(), name='verify-user'),
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('call/<int:pk>/', ChatPageView.as_view(), name='chat-page'),
    path('invite-user/', InviteUserView.as_view(), name='invite_user'),
    path('connect-user/<int:pk>', ConnectUser.as_view(), name='connect-user'),
    # path('chat-history/<int:room>', ChatHistory.as_view(), name='chat-history'),/(?P<room_name>[\w\+]+),
    
    path('room-add-person/', AddToRoomView.as_view(), name='add-to-room'),
    path('room-leave/', LeaveRoomView.as_view(), name='leave-room'),
    path('room-remove-person/', RoomRemovePersonView.as_view(), name='room-remove-person')
]