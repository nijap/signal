from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import TemplateView
from django.contrib import messages
# Create your views here.
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import FormView, RedirectView, View, CreateView
from web_app.models import *
from web_app.serializers import *
from web_app.forms import UserLoginForm, InviteUserForm
from web_app.utils import send_invitation
from django.contrib.auth import  authenticate, login, logout
from django.core.signing import Signer
# from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
# Create your views here.
from accounts.models import MyUser as User
from accounts.models import *

class Dashboard(TemplateView):
    # template_name = 'call-connect.html'
    template_name = 'video_call.html'

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        # remote_user_id = kwargs.get('pk', '')
        # user_id = self.request.user.id
        # remote_user_id = User.objects.get(id=remote_user_id).id
        # room_number = int(user_id)+int(remote_user_id)
        try:
            __chatroom = ChatRoom.objects.get_or_create(room_name='test_room')
        except:
            print("except")
        context['room_number'] = 'test_room' #room_number
        return context


class LoginView(FormView):
    template_name = 'index.html'
    form_class = UserLoginForm
    success_url = '/'
    permission_classes = []

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            return HttpResponseRedirect('/dashboard')
        return self.render_to_response(context)

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        context = {}

        if user:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect('/dashboard')
            else:
                context['form'] = form
                context['error'] = 'Inactive'
                return render(self.request, self.template_name, context)
        else:
            context['form'] = form
            context['error'] = 'Invalid credentials'
            return render(self.request, self.template_name, context)



class Home(TemplateView):
    template_name = 'contact-list.html'

    def online_users_list(self):
        sessions = Session.objects.filter(expire_date__gte=timezone.now())
        uid_list = []        
        for session in sessions:
            data = session.get_decoded()
            uid_list.append(data.get('_auth_user_id', None))
        
        return User.objects.filter(id__in=uid_list).exclude(id=self.request.user.id)

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['users'] = self.online_users_list()
        return context


class ChatPageView(TemplateView):
    # template_name = 'call-connect.html'
    template_name = 'video_call.html'


    def get_context_data(self, **kwargs):
        context = super(ChatPageView, self).get_context_data(**kwargs)
        remote_user_id = kwargs.get('pk', '')
        user_id = self.request.user.id
        remote_user_id = User.objects.get(id=remote_user_id).id
        room_number = int(user_id)+int(remote_user_id)
        context['room_number'] = room_number
        return context


class InviteUserView(FormView):
    form_class = InviteUserForm
    template_name = 'invite_user.html'
    success_url = '/invite_user'

    def form_valid(self, form):
        email = form.cleaned_data['email']
        sender_mail = self.request.user
        #password = str(email.split('@')[0])+'@123'
        #print("password : ", password)

        #new_user, created = InviteMember.objects.get_or_create(sender=self.request.user,receiver=email)
        #new_user.save()

        signer = Signer()
        signed_value = signer.sign(email)
        key = ''.join(signed_value.split(':')[1:])

        # while True:
        #     if ActivationKey.objects.filter(key=key).exists():
        #         key = uuid.uuid4().hex[:6].upper()
        #     else:
        #         break

        #ActivationKey.objects.create(user=email, key=key)

        mail_status = send_invitation(self, email, key)

        if mail_status:
            InviteMember.objects.create(
                sender= sender_mail,
                receiver=email)
            ActivationKey.objects.create(user_email=email, key=key)
            messages.success(self.request, 'Succesfully sent mail')
        else:
            messages.error(self.request, 'Couldint sent mail.')
        return super(InviteUserView, self).form_valid(form)


class VerifyUserKeyView(View):

    def get(self, request, *args, **kwargs):
        user_key = self.kwargs.get('user_key', '')
        print("Key : ", user_key)
        if ActivationKey.objects.filter(key=user_key).exists():
            activation_obj = ActivationKey.objects.get(key=user_key)
            user_email = activation_obj.user_email
            #new_user.is_active = True
            # new_user.save()
            try:
                invitation_object = InviteMember.objects.get(receiver=user_email)
                invitation_object.is_accept = True
                invitation_object.save()
            except InviteMember.DoesNotExist:
                invitation_object = None
                
            # member, created = Member.objects.get_or_create(user=user_email)
            # member.name = new_user.username.split("@")[0]
            # member.active = True.
            # member.save()
            return HttpResponseRedirect('/')
        return HttpResponse("wrong key")


# -------------------------API------------------------------------

from rest_framework.generics import GenericAPIView, UpdateAPIView
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from accounts.authentication import TokenAuthentication
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework import status, generics

class ConnectUser(GenericAPIView):
    # permission_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        remote_user_id = self.kwargs.get('pk', '')
        tocken = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        token_obj = Token.objects.get(key=tocken)
        user = token_obj.user
        try:
            remote_user = MyUser.objects.get(id=remote_user_id)
        except MyUser.DoesNotExist:
            return Response('No User found for "'+str(remote_user_id)+'"', status=HTTP_400_BAD_REQUEST)
        chatroom = ChatRoom.objects.filter(members__id=user.id, is_group=False).filter(members__id=remote_user_id)
        if not len(chatroom) > 0:
            room = 'room_'+str(user.id)+'_'+str(remote_user_id)
            chatroom = ChatRoom.objects.create(room_name=room)
            chatroom.members.add(user,remote_user)
            chatroom.save()
            return Response({'room':chatroom.room_name})
        else:
            return Response({'room':chatroom[0].room_name})


# class ChatHistory(ModelViewSet):
#     queryset = []
#     serializer_class = ChatHistorySerializer

    # def get_queryset(self):
    #     # room_id = self.request.GET.get('room_id')
    #     room_name = 1
    #     if room_id:
    #         self.queryset = ChatHistory.objects.filter(room_name=room_name)
    #         return self.queryset
    #     return self.queryset


class UsersList(ModelViewSet):
    queryset = MyUser.objects.filter(is_admin=False)
    serializer_class = UserListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        self.queryset = MyUser.objects.filter(is_admin=False)
        return self.queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset:
            return Response('No Users found', status=HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class RoomCreateView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = CreateRoomSerializer
    # http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        tocken = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        token_obj = Token.objects.get(key=tocken)
        user = token_obj.user
        room = request.data.post('room_name', '')
        chatroom = ChatRoom.objects.get_or_create(room_name=room)
        chatroom.members.add(user)
        chatroom.is_group = True
        chatroom.save()
        return Response({'room':chatroom.room_name, 'id' :chatroom.id })


class AddToRoomView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = AddToRoomSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user_id = serializer.data.get('user_id', '')
            room_id = serializer.data.get('room_id', '')
            if user_id and room_id:
                try:
                    person = MyUser.objects.get(id=user_id)
                except MyUser.DoesNotExist:
                    return Response('No User found', status=HTTP_400_BAD_REQUEST)
                try:
                    chatroom = ChatRoom.objects.get(id=room_id)
                    chatroom.members.add(person)
                    chatroom.save()
                    members_list = []
                    for user in chatroom.members.all():
                        user_dict = {'id' : user.id, 'email': user.email}
                        members_list.append(user_dict)                        
                    return Response({'id' : chatroom.id,
                        'room_name':chatroom.room_name,
                        'members' : members_list
                        })
                except ChatRoom.DoesNotExist:
                    return Response('No Room found', status=HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeaveRoomView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        room_id = request.data.get('room_id', '')
        tocken = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        token_obj = Token.objects.get(key=tocken)
        myuser = token_obj.user
        if myuser and room_id:
            try:
                chatroom = ChatRoom.objects.get(id=room_id)
                chatroom.members.remove(myuser)
                chatroom.save()
                members_list = []
                for user in chatroom.members.all():
                    user_dict = {'id' : user.id, 'email': user.email}
                    members_list.append(user_dict)
                return Response({'room':chatroom.room_name,
                    'id' : chatroom.id,
                    'members' : members_list
                    })
            except ChatRoom.DoesNotExist:
                return Response('No Room found', status=HTTP_400_BAD_REQUEST)
        return Response('No Roomxx found', status=HTTP_400_BAD_REQUEST)
            

class RoomRemovePersonView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        room_id = request.data.get('room_id', '')
        user_id = request.data.get('user_id', '')
        if user_id and room_id:
            try:
                person = MyUser.objects.get(id=user_id)
            except MyUser.DoesNotExist:
                return Response('No User found', status=HTTP_400_BAD_REQUEST)
            try:
                chatroom = ChatRoom.objects.get(id=room_id)
                chatroom.members.remove(person)
                chatroom.save()
                members_list = []
                for user in chatroom.members.all():
                    user_dict = {'id' : user.id, 'email': user.email}
                    members_list.append(user_dict)
                return Response({'room':chatroom.room_name,
                    'id' : chatroom.id,
                    'members' : members_list
                    })
            except ChatRoom.DoesNotExist:
                return Response('No Room found', status=HTTP_400_BAD_REQUEST)
        return Response('No Roomxx found', status=HTTP_400_BAD_REQUEST)


class RoomMessagesView(ModelViewSet):
    queryset = ChatHistory.objects.all()
    serializer_class = RoomMessagesSerializer
    # lookup_field = 'room__id'

    def get_queryset(self):
        room_name = self.request.GET.get('room_name')
        if room_name:
            self.queryset = ChatHistory.objects.filter(room__room_name=room_name)
            return self.queryset
        return []


class RoomDetailView(ModelViewSet):
    queryset = ChatRoom.objects.all()
    serializer_class = RoomDetailSerializer
    lookup_field = 'id'

    def get_queryset(self):
        room_id = self.request.GET.get('id')
        if room_id:
            self.queryset = ChatRoom.objects.filter(id=room_id)
            return self.queryset
        return self.queryset


