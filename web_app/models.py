from django.db import models
# from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.utils import timezone
from accounts.models import MyUser as User
# Create your models here.


class ActivationKey(models.Model):
	user_email = models.CharField(max_length=50, blank=True, null=True)
	key = models.CharField(max_length=100)


class Member(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=100, null=True, blank=True)
	active = models.BooleanField(default=False)

	@receiver(post_delete, sender='web_app.Member')
	def increment_roomtype(instance, **kwargs):
		instance.user.delete()
	
	def __str__(self):
		return self.name


class InviteMember(models.Model):
	sender = models.ForeignKey(User, related_name='user_sender', on_delete=models.CASCADE)
	receiver = models.CharField(max_length=50, blank=True, null=True)
	date = models.DateTimeField(default=timezone.now)
	is_accept = models.BooleanField(default=False)


class CallHistory(models.Model):
    dailer = models.ForeignKey(Member, related_name='caller_id', blank=True, null=True, on_delete=models.CASCADE)
    receiver = models.ForeignKey(Member, related_name='receiver_id', blank=True, null=True, on_delete=models.CASCADE)
    duration = models.TimeField(default=0 )
    call_on_date = models.DateTimeField(blank=True, null=True, default=timezone.now)


class ChatRoom(models.Model):
	room_name = models.CharField(max_length=50)
	members = models.ManyToManyField(User)
	is_group = models.BooleanField(default=False)

	def __str__(self):
		return self.room_name


class ChatHistory(models.Model):
	room = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
	text = models.CharField(max_length=500)
	date = models.DateTimeField(default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.text