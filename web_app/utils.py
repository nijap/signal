from django.core.mail import EmailMessage
from django.conf import settings
# from django.contrib.auth.models import User
from accounts.models import *
from web_app.models import *

def send_invitation(self, email, key):
	print("Key : ", key)
	# url = self.request.build_absolute_uri()
	url = self.request.META.get('REMOTE_ADDR')
	body = 'You are invited to join an application. Please click the link to join : '
	body +=  'https://signalwebapp.herokuapp.com/verify_user_key/' + str(key)
	subject = 'Invitation'
	email = EmailMessage(subject, body, settings.DEFAULT_FROM_EMAIL, (email,))
	email.content_subtype = 'html'
	
	try:
		email.send()
	except Exception as e:
		return  False
	return True


def save_message(room,username, message):
	room_object = ChatRoom.objects.get(room_name=room)
	user = MyUser.objects.get(email=username)
	ChatHistory.objects.create(room=room_object,text=message,user=user)