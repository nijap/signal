from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate

from accounts.serializers import (UserSerializer, LoginSerializer,
        PasswordResetSerializer)
from accounts.models import MyUser, Token
from rest_framework.permissions import AllowAny, IsAuthenticated

from rest_framework.decorators import api_view, permission_classes
from social_django.utils import psa


class UserCreate(APIView):
    """ 
    Creates the user. 
    """
    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            user = MyUser.objects.create_user(email=data['email'],
                 password=data['password'])

            if user:
                token = Token.objects.create(user=user)
                json = {}
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomLogin(APIView):
    """
    Custom Login view
    """
    permission_classes = [AllowAny]
    def post(self, request, format='json'):
        serializer = LoginSerializer(data=request.data)

        if serializer.is_valid():
            data = serializer.data
            user = authenticate(username= data['email'], password=data['password'])

            token = Token.objects.create(user=user)
            json = {}
            json['token'] = token.key
            return Response(json, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class CustomLogoutView(APIView):
    """
    Loging out user from all loged in devices
    """
    def get(self, request, format='json'):

        try:
            tocken = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
            token_obj = Token.objects.get(key=tocken)
            token_obj.delete()
        except:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        return Response({}, status=status.HTTP_200_OK)


class CustomPasswordResetView(APIView):
    """
    Customizing password reset view and sendig email to user
    """
    def post(self, request, format='json'):
        serializer = PasswordResetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(request)
            return Response({}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
