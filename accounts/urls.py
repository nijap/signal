from django.urls import path
from accounts import views
from django.contrib.auth import views as auth_views


urlpatterns = [
	path('account-create/', views.UserCreate.as_view(), name='account_create'),
	path('login/', views.CustomLogin.as_view(), name='login'),
	path('logout/', views.CustomLogoutView.as_view(), name='logout'),
	path('password-reset/', views.CustomPasswordResetView.as_view(), name='password_reset'),
	path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
]