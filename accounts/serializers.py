from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from accounts.models import MyUser
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404
from django.contrib.auth.forms import PasswordResetForm
from django.conf import settings


class UserSerializer(serializers.Serializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=MyUser.objects.all())]
        )

    password = serializers.CharField(min_length=8, style={'input_type': 'password'})
    confirm_password = serializers.CharField(min_length=8, style={'input_type': 'password'})

    def validate(self, data):
        confirm_password = data['confirm_password']
        password = data['password']

        if password and confirm_password and confirm_password != password:
             raise serializers.ValidationError("Passwords don't match")

        return data


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, data):
        email = data['email']
        password = data['password']

        if email and password:
            user = authenticate(username=email, password=password)

            if not user and not MyUser.objects.filter(email=data['email']).exists():
                user = MyUser.objects.create_user(email=data['email'],
                 password=data['password'])

            if user:
                if not user.is_active:
                    raise serializers.ValidationError(_('User account is disabled, please verify your email'))
            else:
                try:
                    user = get_object_or_404(MyUser, email=email)
                except:
                    raise serializers.ValidationError(_('Invalid Login details'))

                if not user.is_active:
                    raise serializers.ValidationError(_('User account is disabled, please verify your email'))

                raise serializers.ValidationError(_('Invalid Login details'))
        else:
            raise serializers.ValidationError(_('Must include "email" and "password".'))

        return data


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = serializers.EmailField()

    password_reset_form_class = PasswordResetForm

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(data=self.initial_data)

        if not self.reset_form.is_valid():
            raise serializers.ValidationError(_('Invalid e-mail address'))

        if not MyUser.objects.filter(email=value).exists():
            raise serializers.ValidationError(_('e-mail address not exists'))

        return value

    def save(self, request):
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }
        self.reset_form.save(**opts)
